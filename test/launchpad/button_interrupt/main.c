#include <stdbool.h>
#include "board.h"
#include "delay.h"
#include "gpio.h"

#define EX_BUTTON	P1_4

gpio_pin_t led;
gpio_pin_t led2;
gpio_pin_t button;
gpio_pin_t ex_button;

bool on_gpio_irq(PinId pin)
{
	switch (pin)
	{
		case BUTTON		: pin_toggle(&led);  break;
		case EX_BUTTON	: pin_toggle(&led2); break;
		default: ;
	}

	return false;
}

int main(void) {
	Board_Init();
	pin_init(&led, LED1, DIR_OUTPUT);
	pin_init(&led2, LED2, DIR_OUTPUT);
	pin_init(&button, BUTTON, DIR_INPUT);
	pin_init(&ex_button, EX_BUTTON, DIR_INPUT);

	pin_set_mode(&button, MODE_PULLUP);
	pin_set_mode(&ex_button, MODE_PULLUP);

	gpio_set_irq_handler(&on_gpio_irq);	
	pin_attach_interrupt(&button, EDGE_FALLING);
	pin_attach_interrupt(&ex_button, EDGE_FALLING);

	LPM3;
	
	return 0;
}
